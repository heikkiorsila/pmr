# Introduction

pmr is a tool for measuring data rate on the UNIX command line. It is
licensed under Public Domain, and therefore its source code can be
freely used for any purpose without restrictions.  It has following
features:

* **Measure data rate** on the command line. pmr reads data from standard
input and copies it to standard output.

* **Limit data rate** to a specified speed (e.g. 100 KiB/s useful for slow
internet connections)

Example: copy files to another host with at most 100 KiB/s speed
```
tar cv *files* |pmr -l 100KiB |nc -q0 host port
```

* **Compute an md5sum** of the stream (useful for verifying integrity of
network transfers)

Example: copy files to another host and verify checksums on both sides

```
Sender: tar cv *files* |pmr -m |nc -q0 host port
```

```
Receiver: nc -l -p port |pmr -m |tar xv
```

* **Calculate time estimate** of the copied data when the stream size is known

Example: copy files to another host and calculate an estimated time of completion

```
tar cv *files* |pmr -s 1GiB |nc -q0 host port
```

# Installation

$ ./configure
$ make

Then, as root or another appropriate user, run:

$ make install

Done.

Optionally, you may manually build a statically linked pmr and install it:

$ make pmr-static && install pmr-static /usr/local/bin/
